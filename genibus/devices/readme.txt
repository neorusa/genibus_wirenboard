В этой папке лежат конфиги оборудования.

Правило наименования:
НАЗВАНИЕ_ОБОРУДОВАНИЯ.json

Структура файла *.json:

[
  [
    "название характеристики устройства",            // - короткое название, типа "i_mo"
    КЛАСС,                                                // - (число) класс передачи данных**
    ID,                                                    // - (число) ID показателя, genispec.pdf (4 раздел, таблица "The Unit Table")
    ACCESS,                                               // - (число) Тип доступа: 1 - READ, 2 - WRITE, 3 - READ&WRITE
    "Развернутое описание"
  ],
  [
    ....
  ],

...
]


**
Класс передачи данных описан в genispec.pdf, раздел 3.2, таблица 2