## Скрипт управления насосами Grundfos для контроллера WirenBoard
1) Загружаем gb.tar.gz на wirenboard, в папку ~/gb/

   - Сначала перейдите в папку, где лежит файл gb.tar.gz
    
        cd PATH_TO_GB.TAR.GZ/
        scp gb.tar.gz root@10.8.0.3:~/gb/gb.tar.gz

2) Подключаемся к WB.
    
    Перейдем в рабочую папку.
    
        cd gb
        
3) Распаковываем архив:
        
        tar -xf gb.tar.gz
4) Изменяем в конфиге номер WB в разделе 'id':

        nano ~/gb/genibus/config/gb.conf
5) Пробуем остановить скрипт:
        
        /etc/init.d/genibus stop
        
    Если произошла ошибка, значит, скрипт не работает. Удалим pid файл:
    
        rm /var/run/genibus/genibus.pid
6) Копируем обновленный скрипт автозапуска:
        
        cp ~/gb/daemon/genibus /etc/init.d/genibus
7) Запускаем скрипт:
        
        /etc/init.d/genibus start
8) Обновим автозапуск
        
        update-rc.d genibus defaults
9) Profit!