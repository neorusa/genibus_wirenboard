#!/usr/bin/python3
# -*- coding: utf-8 -*-
# Author: Ruslan Gareev (neorusa@ya.ru)
#
from collections import namedtuple
import time
import os
import genibus.apdu as apdu
import genibus.gbdefs as defs
from genibus.datamanager.data_items import data_items
from genibus.datamanager.interpreter import interpreter
from genibus.devices.db import DeviceDB
from genibus.linklayer.parser import parse
from genibus.linklayer.serialport import SerialPort
from genibus.utils.logger import Logger
from genibus.config.config import Settings
from decimal import Decimal
from pyModbusTCP.client import ModbusClient
from onewire.onewire import OneWire
import subprocess

# =========================================
# Конфигурация
# =========================================
# Path to configuration file
CONFIG_FILE = 'genibus/config/gb.conf'

# Get configuration
config = Settings(CONFIG_FILE)
if len(config.error) > 0:
    print(config.error)
    raise SystemExit(1)

# SerialPort name
PORT_NAME = config.serial

LOG_FILE = config.logfile

# Адрес нашего WirenBoard по RS-485
SOURCE_ADDRESS = 1

# Адрес MP-204
DESTINATION_ADDRESS = 0

# База данных
db = DeviceDB()

# Logger
logger = Logger(level=config.log_level, filename=LOG_FILE)
logger.info('Logger was started')

# Структура Инфо-ответов от устройтсва
Info = namedtuple('Info', 'head unit zero range')

# Получим список команд
COMMANDS = db.dataitemsByClass('mp204', defs.APDUClass.COMMANDS)

# Рабочий список адресов параметров (вольтаж, температура, токи и т.д.)
DATA_ITEMS = data_items


# =========================================
# Вспомогательные функции
# =========================================


# Функция отправки запросов к устройству
def send_request(data):
    conn.write(data)
    time.sleep(0.055)   # Макс 55мс до того как 204-й начнет отвечать, иначе получим пустой ответ
    answer = conn.read()
    if answer == b'':
        raise Exception('MP-204 did not responded')
    return answer


# Получим адрес подключенного устройства
def get_address():
    poll_request = apdu.createConnectRequestPDU(SOURCE_ADDRESS)
    response = send_request(poll_request)
    logger.debug('MP-204 response on SOURCE_ADDRESS: %s' % response)
    response = parse(response)
    unit = response.sa
    logger.info('MP-204 address: %s' % unit)
    return unit


# Получим INFO-информацию на каждую характеристику, чтобы позже экстраполировать значения
def get_scaling_info(data):
    logger.info('Taking INFO data for all measurements...')
    result = {}
    for row in data.values():
        logger.debug('Taking INFO for \'%s\' ...' % row[0])
        header = apdu.Header(defs.FrameType.SD_DATA_REQUEST, DESTINATION_ADDRESS, SOURCE_ADDRESS)
        request = apdu.createGetInfoPDU(2, header, measurements=[row[0]])
        response = send_request(request)
        try:
            parsed_response = parse(response)
        except Exception as e:
            logger.error(e)
            raise e
        # Из распарсенного ответа получаем блок с данными
        bin_data = parsed_response.APDUs[0].data
        # добавим данные в result
        sif = bin_data[0] & 0b11
        if sif in (0, 1):
            # Значит, значение этого параметра не экстраполируется
            result[row[0]] = Info(bin_data[0], None, None, None)
        else:
            result[row[0]] = Info(bin_data[0], bin_data[1], bin_data[2], bin_data[3])

    return result


# Получаем данные от устройства запросом OS_GET
def get_data(datapoints):
    header = apdu.Header(defs.FrameType.SD_DATA_REQUEST, DESTINATION_ADDRESS, SOURCE_ADDRESS)
    request = apdu.createGetValuesPDU(defs.APDUClass.MEASURED_DATA, header, measurements=datapoints)
    response = send_request(request)
    try:
        parsed_response = parse(response)
    except Exception as e:
        logger.error(e)
        raise e
    data = parsed_response.APDUs[0].data
    return data


# Отправляем команды
def send_command(commands):
    header = apdu.Header(defs.FrameType.SD_DATA_REQUEST, DESTINATION_ADDRESS, SOURCE_ADDRESS)
    request = apdu.createSetCommandsPDU(header, commands)
    response = send_request(request)
    try:
        parsed_response = parse(response)
    except Exception as e:
        logger.error(e)
        raise e
    if len(parsed_response.APDUs[0]) == 0:
        return True
    else:
        return False


# Write values to modbus tcp server
def write_to_server(addr, index, values):
    register_addr = config.offset + (config.id * config.client_base) + (addr * config.device_base) + index
    if mbclient.write_multiple_registers(register_addr, values):
        logger.info('Write to server values=%r starting at reg=%d' % (values, register_addr))
    else:
        logger.error('Error writing to Modbus TCP server at reg=%d' % register_addr)


# Read values from modbus tcp server
def read_from_server(addr, index):
    register_addr = config.offset + (config.id * config.client_base) + (addr * config.device_base) + index
    result = mbclient.read_holding_registers(register_addr)
    try:
        register_val = int(result[0])
    except:
        logger.error('Error reading from Modbus TCP server reg=%d' % register_addr)
        return False
    logger.info('Read from Modbus TCP server reg=%d value=%d' % (register_addr, register_val))
    return register_val


# Главный процесс опроса МР204
def poll_genibus():
    global DESTINATION_ADDRESS
    # получим адрес подключенного устройства на шине RS-485
    if DESTINATION_ADDRESS == 0:
        DESTINATION_ADDRESS = get_address()
    # Получим параметры экстраполяции для каждого параметра измерения
    scaling_info = get_scaling_info(DATA_ITEMS)
    measured_values = {}
    params = []
    # Одно устройство, поэтому порядковый номер равен 0
    addr = 0

    # Считаем все параметры, определенные в файле genibus/datamanager/data_items.py
    for item in DATA_ITEMS:
        data_list = get_data(DATA_ITEMS[item])
        items_to_values_list = zip(DATA_ITEMS[item], data_list)
        # Интерпретируем результаты
        normal_values = interpreter(items_to_values_list, scaling_info, item)
        measured_values[item] = normal_values.value
        logger.debug(item + ': ' + str(normal_values.value) + ' ' + normal_values.unit)

    # Считаем параметры с датчика 1-Wire
    temp = ow.get_temperature()
    measured_values['t_mo2'] = temp
    logger.debug('t_mo2: ' + str(temp) + ' C')

    # Теперь создадим список значений, в соотвествии с порядком следования
    for index, parameter in enumerate(config.devices[addr].read):
        value = measured_values[parameter.name]
        if isinstance(value, Decimal):
            value = int(value * 10)
        elif value == 'NA':
            value = 0xFFFF
        params.insert(index, value)
        logger.debug(str(index) + ': ' + parameter.name + ' - ' + str(measured_values[parameter.name]))

    # Запишем данные на сервер Modbus TCP
    write_to_server(addr, config.read_offset, params)

    # Прочитаем команду для двигателя
    start_state = read_from_server(addr, config.write_offset)
    stop_state = read_from_server(addr, config.write_offset + 1)
    command = None
    if isinstance(start_state, int):
        if start_state == 1 and measured_values['act_mode1'] == 0:
            command = 'RESET_ALARM'
    if isinstance(stop_state, int):
        if stop_state == 1 and measured_values['act_mode1'] == 1:
            command = 'TRIP_A'

    if command:
        command_send_result = send_command([command])
        msg = 'Command %s was return status %s' % (command, str(command_send_result))
        if command_send_result:
            logger.info(msg)
        else:
            logger.error(msg)

    # Обнулим значения командных регистров на серверe Modbus TCP
    write_to_server(addr, config.write_offset, [0, 0])

# =======================================================================
# #############              Подключения             ####################
# =======================================================================


# Create modbus tcp client instance
mbclient = ModbusClient(host=config.ipaddress, port=config.tcpport, timeout=5.0, auto_open=True, auto_close=True)

# Пробуем подключится к порту, на котором RS-485,
# если не получится, то завершаем работу.
conn = SerialPort(PORT_NAME)
try:
    conn.connect()
except:
    logger.critical('Cannot connect to serial port %s' % PORT_NAME)
    raise SystemExit(1)

# Connect to 1Wire devices
ow = OneWire()
# ================================================
# Основная функция программы
# ================================================


status = True
first_start = True
errors_count = 0
while status:
    # Если куча ошибок, то отправим в ребут
    if errors_count > 5:
        subprocess.call('reboot -i')
    try:
        poll_genibus()
        errors_count = 0
    except Exception as e:
        logger.error(str(e))
        errors_count += 1
    time.sleep(config.polling_interval)
    # Для теста. В проде - ставить True
    status = True

