# -*- coding: utf-8 -*-
# Author: Ruslan Gareev (neorusa@ya.ru)
# Здесь не все параметры, а только нужные для передачи на SCADA
# Остальные параметры можно узнать из документации, или в файле ../genibus/devices/mp204.json

data_items = {
    'v_12': ['v_12_hi', 'v_12_lo'],
    'v_23': ['v_23_hi', 'v_23_lo'],
    'v_31': ['v_31_hi', 'v_31_lo'],
    'v_line': ['v_line_hi', 'v_line_lo'],
    'i1': ['i1_hi', 'i1_lo'],
    'i2': ['i2_hi', 'i2_lo'],
    'i3': ['i3_hi', 'i3_lo'],
    'i_line': ['i_line_hi', 'i_line_lo'],
    'i_line_start': ['i_line_start_hi', 'i_line_start_lo'],
    't_mo1': ['t_mo1'],
    'i_asym': ['i_asym'],
    'cos_phi': ['cos_phi'],
    'r_insulate': ['r_insulate'],
    'act_mode1': ['act_mode1'],
    'alarm_code': ['alarm_code'],
}
