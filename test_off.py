# -*- coding: utf-8 -*-
# Author: Ruslan Gareev (neorusa@ya.ru)

import genibus.apdu as apdu
import genibus.gbdefs as defs
from genibus.linklayer.parser import parse
from genibus.linklayer.serialport import SerialPort
import time
from genibus.devices.db import DeviceDB
import genibus.utils.bytes as bytes
from genibus.config.config import Settings

# db = DeviceDB()
# result = db.dataitemsByClass('mp204', 3)

# print(result)
# for row in result:
#     print(result[row])
# print(result['p_hi'][2])


# Path to configuration file
CONFIG_FILE = 'genibus/config/gb.conf'


def send_request(data):
    conn.write(data)
    time.sleep(0.055)   # Макс 55мс до того как 204-й начнет отвечать, иначе получим пустой ответ
    answer = conn.read()
    return answer


request = apdu.createConnectRequestPDU(1)


conn = SerialPort('/dev/ttyAPP1')
conn.connect()

ans = send_request(request)
print('conn request answer', ans)
unit = parse(ans)
sa = unit.sa

header = apdu.Header(defs.FrameType.SD_DATA_REQUEST, sa, 1)
req = apdu.createSetCommandsPDU(header, ['TRIP_A'])
time.sleep(1)
ans2 = send_request(req)

parsed_answer = parse(ans2)
print('after command send answer', parsed_answer)
