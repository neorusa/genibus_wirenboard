# -*- coding: utf-8 -*-
# Author: Ruslan Gareev (neorusa@ya.ru)

import ast
import copy


# Convert nesting dictionaries into Struct objects
class Struct:
    def __init__(self, **params):
        self.__dict__ = self.convert_dict(params)

    def convert_dict(self, param):
        result = param
        for key, item in param.items():
            if isinstance(item, dict):
                try:
                    a = Struct(**item)
                except TypeError:
                    a = self.convert_dict(item)
                result[key] = a
            elif isinstance(item, list):
                result[key] = self.convert_list(item)
        return result

    def convert_list(self, param):
        result = param
        for index, item in enumerate(param):
            if isinstance(item, dict):
                try:
                    a = Struct(**item)
                except TypeError:
                    a = self.convert_dict(item)
                result[index] = a
            elif isinstance(item, list):
                result[index] = self.convert_list(item)
        return result


# Load settings from file
class Settings(Struct):
    def __init__(self, filename):
        f = open(filename, 'r')
        s = f.read()
        f.close()
        try:
            d = ast.literal_eval(s)
        except BaseException as err:
            self.error = err.args
        else:
            super().__init__(**d)
            self.device_kinds = self.convert_dict(d['device_kinds'])
            self.error = ()
            # Apply device templates
            for addr, device in self.devices.items():
                for kind, template in self.device_kinds.items():
                    try:
                        if device.kind == kind:
                            self.devices[addr] = copy.deepcopy(template)
                            self.devices[addr].kind = kind
                    except:
                        device.kind = ''
        if len(self.error) > 0:
            msg = 'Error: %s in `%s`' % (self.error[0], filename)
            try:
                msg = msg + (' at line %d' % self.error[1][1])
            except:
                pass
            self.error = msg

