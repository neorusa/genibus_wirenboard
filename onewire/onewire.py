# -*- coding: utf-8 -*-
# Author: Ruslan Gareev (neorusa@ya.ru)
import os
from decimal import Decimal as D

OnewireDevicesPath = '/sys/bus/w1/devices'


class OneWire(object):
    def __init__(self):
        self.devices = self.get_devices()

    def get_devices(self):
        devices = []
        try:
            dir_entries = os.listdir(OnewireDevicesPath)
            if len(dir_entries) > 0:
                for entry in dir_entries:
                    if entry.startswith('28-') or entry.startswith('10-') or entry.startswith('22-'):
                        devices.append(entry)
        except:
            pass
        return devices

    def read_temp(self, device):
        data = None
        CRC_OK = False
        try:
            with open(os.path.join(OnewireDevicesPath, device, 'w1_slave')) as sLines:
                for sLine in sLines:
                    if 'crc=' in sLine:
                        if 'YES' in sLine:
                            CRC_OK = True
                    elif 't=' in sLine:
                        pos = sLine.find('t=')
                        data = sLine[pos + 2:]
        except IOError:
            pass

        if CRC_OK:
            data = int(data)
            if data == 85000:
                # wrong read
                return None
            if data == 127937:
                # returned max possible temp, probably an error
                # (it happens for chinese clones)
                return None
            return D(data) / 1000  # Temperature given by kernel is in thousandths of degrees

        return None

    def get_temperature(self):
        temps = 0
        i = 0
        for device in self.devices:
            temp = self.read_temp(device)
            if temp:
                temps += temp
                i += 1
        try:
            avg_temp = temps / i
        except ZeroDivisionError:
            return 'NA'

        return avg_temp
