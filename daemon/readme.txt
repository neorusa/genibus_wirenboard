
## закинуть файл genibus в папку /etc/init.d/
## выполнить команду:

chmod +x /etc/init.d/genibus


## Затем выполнить команду:

/etc/init.d/genibus start


## можно запускать-останавливать командой:

/etc/init.d/genibus start
/etc/init.d/genibus stop


## Проверять статус скрипта:

/etc/init.d/genibus status

########### =================== ############
## Для того, чтобы скрипт автоматически запускался после перезагрузки WB, выполнить команду:

update-rc.d genibus defaults

## после перезагрузки автоматом будет запускаться
