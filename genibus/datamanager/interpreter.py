# -*- coding: utf-8 -*-
# Author: Ruslan Gareev (neorusa@ya.ru)

from genibus.datamanager.conversion import *
from collections import namedtuple
from genibus.datamanager.data_items import data_items
import genibus.utils.bytes as bytes
from genibus.devices.db import DeviceDB

ReturnValue = namedtuple('ReturnValue', 'value unit')


def interpreter(data, scaling_items, item_name):
    x = []
    # Выпишем все значения
    for device in data:
        x.append(device[1])
    # получим данные для экстраполирования
    info = data_items[item_name][0]
    sc_info = scaling_items[info]

    db = DeviceDB()
    Units = db.units()

    try:
        unit = Units[sc_info.unit - 1]
        # 0.1, 1, 10 - точность
        unit_prefix = unit[2]
        # Единица измерения (Амперы, вольты и тп.)
        unit_Unit = unit[3]
    except:
        unit = ''
        unit_prefix = ''
        unit_Unit = ''

    # Eсли старший байт равен 0хFF, то значит, что значение не доступно
    if x[0] == 255:
        value = 'NA'
        return ReturnValue(value, unit_Unit)

    value = 0

    if item_name == 'alarm_code':
        if x[0] != 0:
            value = x[0]
            alarm = db.getAlarm(value)
            unit_Unit = alarm[1]
        else:
            value = 0
            unit_Unit = 'OK'
    elif item_name == 'warnings_1':
        if x[0] == 0:
            unit_Unit = 'OK'
        else:
            value = x[0]
    elif item_name == 'warnings_2':
        if x[0] == 0:
            unit_Unit = 'OK'
        else:
            value = x[0]
    elif item_name == 'warnings_3':
        if x[0] == 0:
            unit_Unit = 'OK'
        else:
            value = x[0]
    elif item_name == 'act_mode1':
        # Получим только состояние двигателя: работает, или отключен
        # Если мотор электрически включен, то смотрим состояние
        if x[0] & 8 == 0:
            var = x[0] & 7
            if var == 0:
                value = 1
            else:
                value = 0
        else:
            value = 0
    else:
        if len(x) == 1:
            if sc_info.head == 130:
                value = convertForward8(x[0], sc_info.zero, sc_info.range, unit_prefix)
            elif sc_info.head == 131:
                value = convertExtended8(x[0], sc_info.zero, sc_info.range, unit_prefix)
            else:
                value = x[0]
        elif len(x) == 2:
            byte_16 = bytes.makeWord(x[0], x[1])
            if sc_info.head == 130:
                value = convertForward16(byte_16, sc_info.zero, sc_info.range, unit_prefix)
            elif sc_info.head == 131:
                value = convertExtended16(byte_16, sc_info.zero, sc_info.range, unit_prefix)
        elif len(x) == 3:
                value = convertExtended24(x, sc_info.zero, sc_info.range, unit_prefix)
        elif len(x) == 4:
            value = convertExtended32(x, sc_info.zero, sc_info.range, unit_prefix)

    return ReturnValue(value, unit_Unit)

